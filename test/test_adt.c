#include "unity.h"

#include "adt.h"

#define DATASIZE 6

void setUp (void)
{
}

void tearDown (void)
{
}

void test_adt_element_count (void)
{
    TEST_ASSERT_EQUAL_INT(1,  ADT_ELEMENT_COUNT_(1));
    TEST_ASSERT_EQUAL_INT(1,  ADT_ELEMENT_COUNT_(sizeof(ADT_ELEMENT_TYPE_)));
    TEST_ASSERT_EQUAL_INT(2,  ADT_ELEMENT_COUNT_(sizeof(ADT_ELEMENT_TYPE_) + 1));
    TEST_ASSERT_EQUAL_INT(2,  ADT_ELEMENT_COUNT_(sizeof(ADT_ELEMENT_TYPE_) * 2));
}

void test_adt_size (void)
{
    struct { ADT_DATA(1); } testData0;
    struct { ADT_DATA(sizeof(ADT_ELEMENT_TYPE_)); } testData1;
    struct { ADT_DATA(sizeof(ADT_ELEMENT_TYPE_) + 1); } testData2;
    struct { ADT_DATA(sizeof(ADT_ELEMENT_TYPE_) * 2); } testData3;
    TEST_ASSERT_EQUAL_INT(sizeof(ADT_ELEMENT_TYPE_) * 1, sizeof(testData0));
    TEST_ASSERT_EQUAL_INT(sizeof(ADT_ELEMENT_TYPE_) * 1, sizeof(testData1));
    TEST_ASSERT_EQUAL_INT(sizeof(ADT_ELEMENT_TYPE_) * 2, sizeof(testData2));
    TEST_ASSERT_EQUAL_INT(sizeof(ADT_ELEMENT_TYPE_) * 2, sizeof(testData3));
}

void test_adt_dereference (void)
{
    typedef struct TestData { 
        int x[3];
        union {
            ADT_DATA(10);
            void* testDataLocation;
        };
    } TestData;

    typedef struct TestData_ {
        int data;
    } TestData_;

    TestData testData;

    TEST_ASSERT_EQUAL_PTR(&testData.testDataLocation, &(ADT(TestData, &testData)->data));
}  

