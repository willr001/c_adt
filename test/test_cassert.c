/**
 * ## Holds a series of ints
 *
 * == Test List
 *
 * create buffer
 * put item in
 * get item out
 * can't put in full buffer
 * can't get from empty buffer
 * can't put/get from non-existant buffer
 *
 */


#include "unity.h"

#include "cassert.h"

void setUp (void) 
{

}

void tearDown (void) 
{

}

void test_pass (void)
{
    CASSERT(1);
}

void test_manually_cause_compile_failure (void)
{
    TEST_IGNORE();
    //CASSERT(0); // uncommenting this line should cause a compile failure
}

