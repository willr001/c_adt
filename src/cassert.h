/*
 * compile_assert.h
 *
 * Created: 8/15/2016 5:16:46 AM
 *  Author: William.Reynolds
 */ 
#ifndef CASSERT_H_
#define CASSERT_H_

#include "cassert_private.h"

#define CASSERT(X) CASSERT_(X, __LINE__)

#endif
