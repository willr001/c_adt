#ifndef ADT_PRIVATE_H_
#define ADT_PRIVATE_H_

//type of element that hides private data
#define ADT_ELEMENT_TYPE_ void*

//macro to determine how many elements needed to store given bytes
#define ADT_ELEMENT_COUNT_(size) \
    ((size + sizeof(ADT_ELEMENT_TYPE_) - 1) / sizeof(ADT_ELEMENT_TYPE_))

#endif
