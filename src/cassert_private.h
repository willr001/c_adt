#ifndef CASSERT_PRIVATE_H_
#define CASSERT_PRIVATE_H_

#define CASSERT_(X,L) CASSERT2_(X, L)
#define CASSERT2_(X,L) typedef char compile_assert_fail_line_##L[(!!(X))*2-1];

#endif