#ifndef ADT_H_
#define ADT_H_

#include "adt_private.h"

//use to declare space that will hold private data
#define ADT_DATA(size) \
    ADT_ELEMENT_TYPE_ adt_data_[ADT_ELEMENT_COUNT_(size)]

//use to access private data. Assumes type defining the private data is named baseType_.
#define ADT(baseType, pStruct) \
    ((baseType##_*)&((pStruct)->adt_data_))

//use to check size requirements relative to the actual hidden data.
//hopefully not a negative number
#define ADT_SURPLUS(baseType,sizeUsed) \
    (ADT_ELEMENT_COUNT_(sizeUsed) - ADT_ELEMENT_COUNT_(sizeof(baseType##_)))

//call once to make sure that reserved size is enough
#define ADT_CHECK(baseType,sizeUsed) \
    ASSERT(ADT_SURPLUS(baseType,sizeUsed) >= 0)

//call once to make sure that reserved size is perfect
#define ADT_STRICT_CHECK(baseType,sizeUsed) \
    ASSERT(ADT_SURPLUS(baseType,sizeUsed) == 0)

#endif
