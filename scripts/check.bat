::    --enable=<id>        Enable additional checks. The available ids are: Several ids can be given if you separate them with commas. See also --std
::                          * all
::                          * warning
::                          * style
::                          * performance
::                          * portability
::                          * information
::                          * unusedFunction
::                          * missingInclude
::    --library=<cfg>      Load file <cfg> that contains information about types and functions. 
::    --project=<file>     Run Cppcheck on project. The <file> can be a Visual Studio Solution (*.sln), Visual Studio Project
::                         (*.vcxproj), or compile database (compile_commands.json). The files to analyse,
::    -rp, --relative-paths
..\vendor\cppcheck\cppcheck.exe -x c --inline-suppr -j 4 --platform=win32A --template=gcc --std=c99 -I vendor/ceedling/vendor/unity/src "%~dp0..\test"
..\vendor\cppcheck\cppcheck.exe -x c --inline-suppr -j 4 --platform=win32A --template=gcc --std=c99 "%~dp0..\src"
